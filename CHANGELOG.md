This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

# Changelog for "seadatanet-connector"

## [v1.2.4-SNAPSHOT] - 2022-12-06

- Updated bom to latest version [#24209]


## [v1.2.3] - 2021-04-26

- update to dismiss log4j [#21268]


## [v1.2.2] - 2021-01-26

- update ecological-engine-geospatial-extensions lower bound range


## [v1.2.1] - 2020-06-10

- Updated for support https protocol [#19423]


## [v1.1.0] - 2016-10-03

- First Release



