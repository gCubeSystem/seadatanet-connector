package org.gcube.dataanalysis.seadatanet;

public class DivaFitResponse {

	String identifier; 
	public String getIdentifier() {
		return identifier;
	}




	public void setIdentifier(String identifier) {
		this.identifier = identifier;
	}




	public Double getLength_scale() {
		return length_scale;
	}




	public void setLength_scale(Double length_scale) {
		this.length_scale = length_scale;
	}




	public Double getSignal_to_noise() {
		return signal_to_noise;
	}




	public void setSignal_to_noise(Double signal_to_noise) {
		this.signal_to_noise = signal_to_noise;
	}




	public Double getVariance_of_background() {
		return variance_of_background;
	}




	public void setVariance_of_background(Double variance_of_background) {
		this.variance_of_background = variance_of_background;
	}




	public Double getFit_quality() {
		return fit_quality;
	}




	public void setFit_quality(Double fit_quality) {
		this.fit_quality = fit_quality;
	}




	public String getPlot() {
		return plot;
	}




	public void setPlot(String plot) {
		this.plot = plot;
	}




	Double length_scale; 
	Double signal_to_noise; 
	Double variance_of_background; 
	Double fit_quality; 
	String plot; 

	public DivaFitResponse(String identifier, 
	Double length_scale,
	Double signal_to_noise, 
	Double variance_of_background, 
	Double fit_quality, 
	String plot){
		
		super();
		this.identifier=identifier;
		this.length_scale=length_scale;
		this.signal_to_noise=signal_to_noise;
		this.variance_of_background=variance_of_background;
		this.fit_quality=fit_quality;
		this.plot=plot;
	}

	

	
	@Override
	public String toString() {
		return "DivaFitResponse [ IDFILE="+ identifier +", length_scale=" + length_scale + ", signal_to_noise="
				+ signal_to_noise + ", variance_of_background=" + variance_of_background + ", fit_quality=" + fit_quality +
				", plot=" + plot 
				+ "]";
	}

}
